package uk.co.sheffieldwebprogrammer.gitlabdemo.todo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class TodoControllerTest {

    @InjectMocks
    private TodoController todoController;

    @Test
    public void get() {
        ResponseEntity<List<Todo>> listResponseEntity = todoController.get();
        assertEquals(listResponseEntity.getBody().size(),2);
    }
}