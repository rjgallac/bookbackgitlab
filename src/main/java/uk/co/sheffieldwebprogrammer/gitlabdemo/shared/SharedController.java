package uk.co.sheffieldwebprogrammer.gitlabdemo.shared;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("shared")
public class SharedController {
    @GetMapping
    public ResponseEntity<String> get() throws InterruptedException {
        Thread.sleep(5000);
        return new ResponseEntity<>("{\"message\":\"shared\"}", HttpStatus.OK);
    }
}
