package uk.co.sheffieldwebprogrammer.gitlabdemo.todo;

public class Todo {
    private String todo;

    public Todo(String todo) {
        this.todo = todo;
    }

    public String getTodo() {
        return todo;
    }
}
