package uk.co.sheffieldwebprogrammer.gitlabdemo.todo;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/todo")
public class TodoController {
    List<Todo> todoList = new ArrayList<>();


    public TodoController() {
        this.todoList.add(new Todo("test"));
        this.todoList.add(new Todo("test 2"));
    }

    @GetMapping
    public ResponseEntity<List<Todo>> get(){
        return new ResponseEntity<>(this.todoList, HttpStatus.OK);
    }
}
