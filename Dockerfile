FROM openjdk:8
COPY build/libs/gitlabdemo-0.0.1-SNAPSHOT.jar .
EXPOSE 8080
CMD ["java", "-jar", "gitlabdemo-0.0.1-SNAPSHOT.jar"]